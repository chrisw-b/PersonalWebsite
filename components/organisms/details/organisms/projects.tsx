import * as React from 'react';

import styled from '@emotion/styled';

import { ProjectsQuery } from '@queries/projects.generated';

import Project from '../molecules/project';

const ProjectList = styled.ul`
  display: flex;
  flex-flow: row wrap;
  list-style: none;
  padding: 0;
`;

type OwnProps = {
  projects: ProjectsQuery;
};

const Projects: React.FC<OwnProps> = ({ projects }) => {
  const projectList = projects.projects ?? [];
  return (
    <ProjectList>
      {projectList.map((project) => (
        <Project key={project?.github ?? undefined} project={project} />
      ))}
    </ProjectList>
  );
};

export default Projects;
