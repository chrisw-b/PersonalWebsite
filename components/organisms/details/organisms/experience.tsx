import * as React from 'react';

import { css } from '@emotion/react';
import styled from '@emotion/styled';

import { JobExperienceQuery } from '@queries/jobExperience.generated';
import rowToColor from 'lib/gradientToHSL';

const ExperienceGrid = styled.div`
  display: grid;
  grid-template: auto / [left-start right-start] 1fr [left-end right-end];
  justify-items: center;
  margin: 0 0 3.125rem;

  @media screen and (min-width: 880px) {
    grid-gap: 1.875rem 3.75rem;
    grid-template: auto / [left-start] 1fr [left-end right-start] 1fr [right-end];
  }
`;

const CompanyName = styled.h3`
  font-family: var(--font-family-header);
  font-size: var(--subhead-text);
  font-style: italic;
  margin: 0;
  padding: 0;
`;

const LeftBorder = css`
  border-right-width: 0.125rem;
  right: -1.9375rem;
`;
const RightBorder = css`
  border-left-width: 0.125rem;
  left: -1.9375rem;
`;

const toGridRowString = (rowStart: number, rowEnd: number) => `${rowStart} / ${rowEnd}`;

const JobItem = styled.div<{
  ColumnSide: 'left' | 'right';
  RowNumber: number;
  TotalRows: number;
}>`
  grid-column: ${(props) =>
    props.ColumnSide === 'left' ? 'left-start / left-end' : 'right-start / right-end'};

  position: relative;

  &::before {
    border-color: hsl(${(props) => rowToColor(props.RowNumber, props.TotalRows)});
    border-style: solid;
    border-width: 0;
    border-top-width: 0.125rem;
    content: '';
    height: 100%;
    position: absolute;
    top: 0.78125rem;
    width: 0.9375rem;

    @media screen and (max-width: 880px) {
      ${RightBorder};
    }
  }

  &:last-of-type {
    &::before {
      border-color: var(--light-grey);
      border-top-color: hsl(${(props) => rowToColor(props.RowNumber, props.TotalRows)});
    }
  }

  @media screen and (min-width: 550px) {
    grid-row: ${(props) => toGridRowString(props.RowNumber + 1, props.RowNumber + 3)};

    &::before {
      ${(props) => (props.ColumnSide === 'left' ? LeftBorder : RightBorder)};
    }
  }
`;

const JobDescription = styled.p`
  font-size: var(--description-text);
`;

const Experience: React.FC<{ jobs: JobExperienceQuery }> = ({ jobs }) => {
  const jobList = jobs.jobs ?? [];
  return (
    <ExperienceGrid>
      {jobList.map((job, i) => (
        <JobItem
          key={job?.company ?? undefined}
          ColumnSide={i % 2 === 1 ? 'right' : 'left'}
          RowNumber={i}
          TotalRows={jobList.length}
        >
          <CompanyName>{job?.company}</CompanyName>
          <JobDescription>
            <b>{`(${job?.when?.start ?? ''} - ${job?.when?.end ?? ''}) `}</b>
            {`${job?.details?.join(' \u00B7 ') ?? ''}`}
          </JobDescription>
        </JobItem>
      ))}
    </ExperienceGrid>
  );
};
export default Experience;
