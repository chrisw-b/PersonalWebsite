import * as React from 'react';
import Markdown from 'react-markdown';

import styled from '@emotion/styled';

import { BioQuery } from '@queries/bio.generated';
import { JobExperienceQuery } from '@queries/jobExperience.generated';
import { ProjectsQuery } from '@queries/projects.generated';
import { Link } from '@styles/css';

import Experience from './organisms/experience';
import Projects from './organisms/projects';

const Overview = styled.p`
  font-size: var(--body-text);
  margin: 0 0 1.25rem;
`;

const DetailHeader = styled.h2`
  font-family: var(--font-family-header);
  font-size: var(--section-header-text);
  font-weight: 700;
  margin: 0;
  text-align: center;
`;

const DetailContent = styled.div`
  :not(:last-of-type) {
    position: relative;

    ::after {
      background-image: linear-gradient(
        to right,
        transparent,
        var(--rose),
        var(--cyan),
        transparent
      );
      bottom: -0.0625rem;
      content: '';
      height: 0.0625rem;
      left: 5%;
      position: absolute;
      right: 5%;
      width: 90%;
    }
  }
`;

const DetailWrapper = styled.article`
  background: var(--light-grey);
  display: grid;
  grid-gap: 1.875rem 0.78125rem;
  grid-template: auto / minmax(auto, 60rem);
  justify-content: center;
  margin: 0 3% 6.25rem;
  padding: 3.125rem 10%;
  transition: all 0.25s var(--bezier-transition);

  @media screen and (max-width: 560px) {
    padding: 1.875rem 5%;
  }
`;

type OwnProps = {
  jobs: JobExperienceQuery;
  projects: ProjectsQuery;
  bio: BioQuery;
};

const Details: React.FC<OwnProps> = ({ bio, jobs, projects }) => {
  return (
    <DetailWrapper>
      <DetailHeader>About</DetailHeader>
      <DetailContent>
        <Markdown
          components={{
            p: (props) => <Overview {...props} />,
            a: (props) => <Link target='_blank' rel='noopener noreferrer' {...props} />,
          }}
        >
          {bio?.bio ?? ''}
        </Markdown>
      </DetailContent>

      <DetailHeader>Experience</DetailHeader>
      <DetailContent>
        <Experience jobs={jobs} />
      </DetailContent>

      <DetailHeader>Personal Projects</DetailHeader>
      <DetailContent>
        <Projects projects={projects} />
      </DetailContent>
    </DetailWrapper>
  );
};

export default Details;
