import * as React from 'react';
import Markdown from 'react-markdown';

import styled from '@emotion/styled';

import { ProjectFragment } from '@queries/projects.generated';
import { Link } from '@styles/css';

const ProjectItem = styled.li`
  align-items: center;
  display: flex;
  flex: 1 1 33%;
  flex-flow: column nowrap;
  margin: 0.625rem 0;
  min-width: 13.125rem;

  a {
    text-decoration: none;
  }
`;

const ProjectDetails = styled.div`
  width: 12.5rem;
`;

const ProjectDescriptionPara = styled.p`
  font-size: var(--description-text);
  padding: 0 0.1875rem;
`;

const TechList = styled.ul`
  display: flex;
  flex-flow: row wrap;
  list-style: none;
  margin: 0.625rem 0 0;
  padding: 0;
`;

const Tech = styled.span`
  font-size: var(--list-text);
  padding: 0 0.1875rem;

  :not(:last-of-type) {
    &::after {
      content: '•';
      display: inline-block;
      padding-left: 0.1875rem;
    }
  }
`;

const TitleWrapper = styled.div`
  background: linear-gradient(
    to bottom,
    var(--light-grey-00),
    var(--light-grey-00) 10%,
    var(--light-grey)
  );
  display: flex;
  flex-flow: column nowrap;
  height: 3.125rem;
  justify-content: flex-end;
  margin: 0.625rem 0 0;
`;
const ProjectTitle = styled.h4`
  color: var(--link-blue);
  font-size: var(--body-text);
  margin: 0 0.1875rem;
`;

const ProjectScreenshot = styled.a<{ imgUrl: string }>`
  background: url('${(props) => props.imgUrl}') center / cover no-repeat scroll;
  border-radius: 0.25rem;
  display: flex;
  flex-flow: column nowrap;
  height: 6.25rem;
  justify-content: flex-end;
  width: 12.5rem;
`;

interface OwnProps {
  project: ProjectFragment | null | undefined;
}

const Project: React.FC<OwnProps> = ({ project }) => {
  return (
    <ProjectItem>
      <div>
        <ProjectScreenshot
          imgUrl={project?.screenshots?.[0] ?? ''}
          href={project?.website ?? undefined}
          target='_blank'
          rel='noopener noreferrer'
        >
          <TitleWrapper>
            <ProjectTitle>{project?.name}</ProjectTitle>
          </TitleWrapper>
        </ProjectScreenshot>
        <ProjectDetails>
          <TechList>
            {project?.technologies?.map((tech) => <Tech key={tech ?? undefined}>{tech}</Tech>)}
          </TechList>
          <Markdown
            components={{
              p: (props) => <ProjectDescriptionPara {...props} />,
              a: (props) => <Link target='_blank' rel='noopener noreferrer' {...props} />,
            }}
          >
            {project?.description ?? ''}
          </Markdown>
          <p>
            <Link href={project?.github ?? undefined} target='_blank' rel='noopener noreferrer'>
              Github
            </Link>
          </p>
        </ProjectDetails>
      </div>
    </ProjectItem>
  );
};
export default Project;
