import * as React from 'react';
import { useInView } from 'react-intersection-observer';

import styled from '@emotion/styled';
import Image from 'next/image';

import { NightModeContext } from '@contexts/nightMode';
import LastFMWidget from '@organisms/widgets/lastfm';
import { LinksQuery } from '@queries/links.generated';
import { usePhotoBlogQuery } from '@queries/photoBlog.generated';
import { Photo } from '@schema/dataModel/personalApi.generated';
import { AutoSeparator, animateImageCss, transparentBg, whiteBg } from '@styles/css';
import LightBulb from 'public/lightbulb.svg';

import Links from './molecules/links';

const DarkModeButton = styled.button`
  background: transparent;
  border: 0;
  cursor: pointer;
  height: 2.25rem;
  margin: 0;
  padding: 0;
  position: absolute;
  right: 0.625rem;
  top: 0.625rem;
  z-index: 100;
`;

const BannerWrapper = styled.div<{ mini: boolean }>`
  align-items: center;
  display: ${({ mini }) => (mini ? 'block' : 'flex')};
  flex-flow: column nowrap;
  height: ${({ mini }) => (mini ? '3.125rem' : '100%')};
  justify-content: center;
  left: 0;
  overflow: hidden;
  position: ${({ mini }) => (mini ? 'absolute' : 'relative')};
  right: 0;
  top: 0;
  transition: all 0.25s var(--bezier-transition);
  z-index: 10;
`;

const ScrollMonitor = styled.div`
  position: absolute;
  top: calc(50vh - 32%);
`;

const BannerPositioner = styled.div<{ mini: boolean }>`
  align-items: center;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  position: relative;
  top: ${({ mini }) => (mini ? 0 : -20)}vh;
  transition: all 0.25s var(--bezier-transition);
  width: 100%;
`;

const Banner = styled.div<{ bgImage: string; mini: boolean; fixed: boolean }>`
  align-items: stretch;
  background-color: var(--white);
  display: flex;
  flex-flow: row nowrap;
  height: ${({ mini }) => (mini ? 3.125 : 12.5)}rem;
  justify-content: stretch;
  position: ${({ mini, fixed }) => (fixed ? 'fixed' : mini ? 'absolute' : 'relative')};
  top: 0;
  transition: all 0.25s var(--bezier-transition);
  width: ${({ mini }) => (mini ? 100 : 75)}vw;
  z-index: 0;

  &::after {
    background: ${({ bgImage }) =>
      bgImage === '' ? 'transparent' : `url(${bgImage}) center / cover no-repeat scroll`};
    bottom: 0;
    content: '';
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    z-index: -1;
    ${({ bgImage }) => bgImage && animateImageCss}
  }

  @media screen and (max-width: 864px) {
    width: 100vw;
  }
`;

const CenterText = styled.div<{ mini: boolean }>`
  ${({ mini }) => (mini ? whiteBg : transparentBg)};
  align-items: center;
  display: flex;
  flex: 1;
  flex-flow: column nowrap;
  position: relative;
  transition: all 0.25s var(--bezier-transition);
  z-index: 10;

  &::before {
    ${({ mini }) => mini && transparentBg}
    bottom: 0;
    content: '';
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    z-index: -1;
  }
`;

const Name = styled.h1<{ mini: boolean }>`
  color: var(--white);
  /* stylelint-disable font-family-name-quotes */
  font:
    700 ${({ mini }) => `var(--page-title-${mini ? 'scroll' : 'large'}) `} 'Open Sans',
    sans-serif;
  margin: 0;
  position: relative;
  top: ${({ mini }) => (mini ? 1.375 : 4.6875)}rem;
  transition: all 0.25s var(--bezier-transition);

  @media screen and (max-width: 910px) {
    font-size: ${({ mini }) => `var(--page-title-${mini ? 'scroll' : 'medium'}) `};
    top: ${({ mini }) => (mini ? 1.4375 : 7.1875)}rem;
  }

  @media screen and (max-width: 560px) {
    font-size: ${({ mini }) => `var(--page-title-${mini ? 'scroll' : 'small'}) `};
    top: ${({ mini }) => (mini ? 1.4375 : 8.59375)}rem;
  }
`;

const Now = styled.div`
  display: flex;
  flex-flow: row nowrap;
  font-size: 0.875rem;
  justify-content: center;
  margin-top: 0.625rem;
  ${AutoSeparator};
`;

const Details = styled.div<{ mini: boolean }>`
  align-items: center;
  display: flex;
  flex-flow: column nowrap;
  height: 2.5rem;
  justify-content: space-between;
  position: relative;
  top: ${({ mini }) => (mini ? -2.03125 : 3.125)}rem;
  transition: all 0.25s var(--bezier-transition);

  @media screen and (max-width: 910px) {
    top: ${({ mini }) => (mini ? -1.75 : 6.25)}rem;
  }

  @media screen and (max-width: 560px) {
    top: ${({ mini }) => (mini ? -1.75 : 7.8125)}rem;
  }
`;

interface OwnProps {
  mini: boolean;
  headerLinks: LinksQuery;
}

const IntroBanner: React.FC<OwnProps> = ({ mini = false, headerLinks }) => {
  const [, toggleLightMode] = React.useContext(NightModeContext);
  const [scrolledRef, allowScroll] = useInView();
  const [bgImage, setBgImage] = React.useState<Photo | null | undefined>(null);
  const { loading, data } = usePhotoBlogQuery();

  React.useEffect(() => {
    const photos = data?.photoBlog?.photos ?? [];
    if (!loading && photos.length > 0) {
      const randomPhotoNumber = Math.floor(Math.random() * photos.length);
      setBgImage(photos[randomPhotoNumber]);
    }
  }, [data?.photoBlog?.photos, loading]);

  return (
    <BannerWrapper mini={!loading && mini}>
      <ScrollMonitor ref={scrolledRef} />

      <BannerPositioner mini={mini}>
        <Banner bgImage={bgImage?.photo ?? ''} mini={mini} fixed={!loading && !allowScroll}>
          <DarkModeButton title='Toggle dark mode' type='button' onClick={toggleLightMode}>
            <span title='bulb icon from noun project'>
              <Image height={36} width={36} src={LightBulb as string} alt='Toggle Dark Mode' />
            </span>
          </DarkModeButton>
          <CenterText mini={mini && !loading}>
            <Name mini={mini && !loading}>Chris Barry</Name>

            <Details mini={mini}>
              <Links mini={mini} links={headerLinks} />
              {!mini ? (
                <Now>
                  <LastFMWidget />
                </Now>
              ) : null}
            </Details>
          </CenterText>
        </Banner>
      </BannerPositioner>
    </BannerWrapper>
  );
};
export default IntroBanner;
