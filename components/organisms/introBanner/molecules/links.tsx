import * as React from 'react';

import styled from '@emotion/styled';

import { LinksQuery } from '@queries/links.generated';
import { AutoSeparator } from '@styles/css';
import gradientAtIndex from 'lib/gradientToHSL';

interface OwnProps {
  mini: boolean;
  links: LinksQuery;
}

export const LinkList = styled.ul<{ mini: boolean }>`
  display: flex;
  flex-flow: row nowrap;
  font-size: var(${({ mini }) => (mini ? '--description-text' : '--subhead-text')});
  font-weight: 500;
  list-style: none;
  margin: 0;
  padding: 0;
  transition: all 0.25s var(--bezier-transition);
  ${AutoSeparator}
  @media screen and (max-width: 736px) {
    font-size: var(--description-text);
  }

  @media screen and (max-width: 608px) {
    font-size: 0.625rem;
  }
`;

const Link = styled.a<{ bgColor: string }>`
  border-left: 0;
  border-right: 0;
  box-shadow: inset 0 -1px 0 hsl(${({ bgColor }) => bgColor}, 60%);
  color: var(--link-blue);
  display: inline-block;
  position: relative;
  text-decoration: none;
  transition: all 0.25s var(--bezier-transition);

  :hover,
  :focus,
  :active {
    box-shadow: inset 0 -20px 0 hsl(${({ bgColor }) => bgColor}, 40%);
  }
`;

const TOTAL_LINKS = 6;

const Links: React.FC<OwnProps> = ({ mini, links }) => {
  return (
    <LinkList mini={mini}>
      <li>
        <Link
          href={links.github?.url ?? undefined}
          target='_blank'
          rel='noopener noreferrer'
          title='Github'
          bgColor={gradientAtIndex(0, TOTAL_LINKS)}
        >
          Github
        </Link>
      </li>
      <li>
        <Link
          href={links.linkedin?.url ?? undefined}
          target='_blank'
          rel='noopener noreferrer'
          title='LinkedIn'
          bgColor={gradientAtIndex(1, TOTAL_LINKS)}
        >
          LinkedIn
        </Link>
      </li>
      <li>
        <Link
          href={links.twitter?.url ?? undefined}
          target='_blank'
          rel='noopener noreferrer'
          title='Twitter'
          bgColor={gradientAtIndex(2, TOTAL_LINKS)}
        >
          Twitter
        </Link>
      </li>
      <li>
        <Link
          href={links.photoBlog?.url ?? undefined}
          target='_blank'
          rel='noopener noreferrer'
          title='Photography'
          bgColor={gradientAtIndex(3, TOTAL_LINKS)}
        >
          Photography
        </Link>
      </li>
      <li>
        <Link
          href={links.lastfm?.url ?? undefined}
          target='_blank'
          rel='noopener noreferrer'
          title='LastFM'
          bgColor={gradientAtIndex(4, TOTAL_LINKS)}
        >
          LastFM
        </Link>
      </li>
      <li>
        <Link
          href='mailto:me@chriswbarry.com'
          target='_blank'
          rel='noopener noreferrer'
          title='Email'
          bgColor={gradientAtIndex(5, TOTAL_LINKS)}
        >
          Email
        </Link>
      </li>
    </LinkList>
  );
};
export default Links;
