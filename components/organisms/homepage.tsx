import * as React from 'react';
import { useInView } from 'react-intersection-observer';

import { ApolloProvider } from '@apollo/client';
import { Global } from '@emotion/react';
import styled from '@emotion/styled';

import { NightModeProvider } from '@contexts/nightMode';
import Description from '@organisms/details';
import IntroBanner from '@organisms/introBanner';
import { BioQuery } from '@queries/bio.generated';
import { JobExperienceQuery } from '@queries/jobExperience.generated';
import { LinksQuery } from '@queries/links.generated';
import { ProjectsQuery } from '@queries/projects.generated';
import globalStyles from '@styles/global';
import apolloClient from 'lib/apollo';

export const PageGrid = styled.div`
  display: grid;
  grid-template: 94vh 0.0625rem max-content / auto;
  overflow: auto;
`;

type OwnProps = {
  headerLinks: LinksQuery;
  jobs: JobExperienceQuery;
  projects: ProjectsQuery;
  bio: BioQuery;
};

const Homepage: React.FC<OwnProps> = ({ headerLinks, jobs, projects, bio }) => {
  const [inViewReference, inView] = useInView({ rootMargin: '-300px 0px 0px' });
  return (
    <ApolloProvider client={apolloClient}>
      <NightModeProvider>
        <Global styles={globalStyles} />
        <PageGrid>
          <div>
            <IntroBanner mini={!inView} headerLinks={headerLinks} />
          </div>
          <div ref={inViewReference} />
          <div>
            <Description jobs={jobs} projects={projects} bio={bio} />
          </div>
        </PageGrid>
      </NightModeProvider>
    </ApolloProvider>
  );
};

export default Homepage;
