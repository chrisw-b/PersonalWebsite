import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client';

import typePolicies from '@schema/typePolicies';

const cache = new InMemoryCache({ typePolicies });

const client = new ApolloClient({
  link: createHttpLink({ uri: process.env.NEXT_PUBLIC_GRAPHQL_API }),
  cache,
});

export default client;
