import type { NextPage } from 'next';

import Homepage from '@organisms/homepage';
import { BioDocument, BioQuery, BioQueryResult } from '@queries/bio.generated';
import {
  JobExperienceDocument,
  JobExperienceQuery,
  JobExperienceQueryResult,
} from '@queries/jobExperience.generated';
import { LinksDocument, LinksQuery, LinksQueryResult } from '@queries/links.generated';
import { ProjectsDocument, ProjectsQuery, ProjectsQueryResult } from '@queries/projects.generated';
import apolloClient from 'lib/apollo';

type OwnProps = {
  headerLinks: LinksQuery;
  jobs: JobExperienceQuery;
  projects: ProjectsQuery;
  bio: BioQuery;
};

const Home: NextPage<OwnProps> = ({ headerLinks, jobs, projects, bio }) => {
  return <Homepage headerLinks={headerLinks} jobs={jobs} projects={projects} bio={bio} />;
};

export const getStaticProps = async () => {
  const { data: headerLinks } = (await apolloClient.query({
    query: LinksDocument,
  })) as LinksQueryResult;
  const { data: jobs } = (await apolloClient.query({
    query: JobExperienceDocument,
  })) as JobExperienceQueryResult;
  const { data: projects } = (await apolloClient.query({
    query: ProjectsDocument,
  })) as ProjectsQueryResult;
  const { data: bio } = (await apolloClient.query({
    query: BioDocument,
  })) as BioQueryResult;

  return {
    props: { headerLinks, jobs, projects, bio },
    revalidate: 60,
  };
};

export default Home;
